import mongoose from "mongoose";
import { MONGO_URI } from "./environment.js";



export default function connectDB(){
    mongoose.set("strictQuery", false);
    return mongoose
      .connect(MONGO_URI)
      .then((succes) => {
        console.log("MongoDB connnected succesfully");
        return true;
      })
      .catch((error) => {
        console.log("MongoDB not conected. Error: ${error}");
        return false;
      });
}