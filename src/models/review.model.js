import mongoose from "mongoose";

const reviewSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    _userId:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    category:{
        type: String,
        required: true
    },
    content:{
        type: String,
        required: true
    }
});

const reviewModel = mongoose.model("Review", reviewSchema);

export default reviewModel;