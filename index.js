import express from "express";
import reviewsRouter from "./src/routes/reviews.auth.js"
import { PORT } from "./src/configs/environment.js";
import connectDB from "./src/configs/mongo.js";

const app = express();

app.use(express.json());

app.use("/reviews", reviewsRouter);

app.listen(3000, ()=>{
    console.log("server started at 3000");
})

app.get("/", function (req, res){
    const ans = ["<p>API desarrollada por Alexis Sanhueza</p>"];
    res.send(ans)
})

async function startSever() {
  const isConnected = await connectDB();
  if (isConnected) {
    app.listen(PORT, () => {
      console.log(`Server started on ${PORT}`);
    });
  } else {
    process.exit();
  }
}

startSever();